import Ajax from './ajax.js'

const utils = {
  $init(vm) {
    Ajax.init(vm)
  }
}

export default {
  install(Vue) {
    Vue.prototype.$utils = utils;
    Vue.prototype.$httpPost = Ajax.httpPost;
  }
}

