import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);

import RouterCache from './modules/router-cache'

export default new Vuex.Store({
  state: {
    token: '666'
  },
  modules: {
    RouterCache
  }
})