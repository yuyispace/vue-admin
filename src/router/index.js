import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import RouterView from './routerView.vue'
import Login from '@views/login/index.vue'

export default new Router({
  //mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '',  redirect: '/login'},
    {
      path: '/login', component: Login
    },
    { path: '*', component: () => import('@views/404.vue')},
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@views/About.vue')
    },
    {
      path: '/index', component: () => import('@views/index/index.vue'),
      children: [
        {
          path: '/', component: RouterView,
          children: [
            {path: '', component: () => import('../views/home/index.vue')}
          ]
        },
        {
          path: '/echarts', component: RouterView,
          children: [
            {path: 'map-chart', component: ()=> import('@views/map-chart/index.vue')}
          ]
        },
        //其它
        {
          path: '/others', component: RouterView,
          children: [
            {path: 'a', component: () => import('@views/others/a.vue')},
            {path: 'b', component: () => import('@views/others/b.vue')},
          ]
        }
      ]
    }
  ]
})
