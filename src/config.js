const SERVER = process.env.SERVER;

//API地址
const apiServer = {
  dev: 'http://dev.sq580.com'
}[SERVER]

export default {
  install(Vue){
    Vue.prototype.$config = {
      apiServer
    };
  }
}
