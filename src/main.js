import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import './assets/styles/index.scss'
import router from './router/index.js'
import store from './store/index.js'

Vue.use(ElementUI, { size: 'small' })

/* 配置文件 */
import Config from './config.js'
Vue.use(Config)

/* utils */
import Utils from './utils/index.js'
Vue.use(Utils)

/* 全局组件 */
import Components from './components/global/index'
Vue.use(Components);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  data() {
    return {
      token: ''
    }
  },
  created() {
    this.$utils.$init(this)
  }
}).$mount('#app')
