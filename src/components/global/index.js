import Page from './page/main.vue'
import Search from './search/main.vue'
import SearchItem from './search/search-item/main.vue'
import TableHeader from './table-header/main.vue'
import Pagination from './pagination/main.vue'


const components = [
	Page,
	Search,
	SearchItem,
	TableHeader,
	Pagination
];

export default {
	install(Vue){
		components.map(component => {
			if (component.install) {
				Vue.use(component)
			} else {
				Vue.component('Vc'+component.name, component);
			}
		});
	}
};