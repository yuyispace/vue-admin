# vue-admin

#### 项目介绍
基于vue的后台管理网站，使用element-ui组件库， [在线demo](http://yuyispace.gitee.io/vue-admin) 

#### 软件架构
vue + vue-router + element-ui + axios

使用vue内置组件 keep-alive 来缓存页面

#### 安装教程

1. 安装：npm install
2. 开发：npm start
3. 打包：npm run build