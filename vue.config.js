/**
 * 调整 webpack 配置
 */
const path = require('path');
module.exports = {
  configureWebpack: config => {
    //console.log(process.env)
    if (process.env.NODE_ENV === 'production') {
      //config.productionSourceMap = false
    } else {
      // 为开发环境修改配置...
    }

    return {
      resolve: {
        alias: {
          '@views': path.join(__dirname, 'src/views')
        }
      },

      // cdn
      /* externals: {
        'vue': 'Vue'
      }, */

      //分割指定模块代码
      /* optimization: {
        splitChunks: {
          cacheGroups: {
            elementUi: {
              test: /[\\/]node_modules[\\/]element-ui[\\/]/,
              name: 'elementUI',
              chunks: 'all',
            }
          }
        }
      } */
    }
  },
  //链式操作
  chainWebpack: config => {
    //全局注册sass变量
    ['vue','vue-modules', 'normal'].forEach(type => {
      config.module.rule('scss').oneOf(type).use('sass-resources').loader('sass-resources-loader').options({
        resources: path.resolve(__dirname, 'src/assets/styles/variable.scss')
      });
    })

    //自定义服务器环境配置信息
    config.plugin('define').tap( args => {
      args[0]['process.env']['SERVER'] = '"'+(process.env.server||'prod')+'"'
      return args
    })
  },
  publicPath: './',   //output.publicPath
  devServer: {
    host: '0.0.0.0'
  }
}